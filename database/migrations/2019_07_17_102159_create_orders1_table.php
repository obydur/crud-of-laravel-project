<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrders1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $table->string('product_id')->nullable();
        Schema::create('orders1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id')->nullable();
            $table->tinyInteger('order_status')->nullable();
            $table->dateTime('order_date')->nullable();
            $table->dateTime('required_date')->nullable();
            $table->dateTime('shipped_date')->nullable();
            $table->integer('store_id')->nullable();
            $table->integer('staff_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders1');
    }
}
