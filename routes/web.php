<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layoutHome.default');
});


//form
Route::resource('/form', 'FormController');

Route::resource('/admin', 'AdminController');
Route::resource('/banner', 'BannerController');
Route::resource('/brand', 'BrandController');
Route::resource('/cart', 'CartController');
Route::resource('/contact', 'ContactController');
Route::resource('/label', 'LabelController');
Route::resource('/category', 'CategoryController');
Route::resource('/page', 'PageController');
Route::resource('/tag', 'TagController');
Route::resource('/testimonial', 'TestimonialController');

//Order

Route::resource('/order', 'OrderController');
Route::resource('/orders1', 'Orders1Controller');
Route::resource('/subscriber', 'SubscriberController');
Route::resource('/sponser', 'SponserController');
Route::resource('/product', 'ProductController');
Route::resource('/map_product_tag', 'Map_product_tagController');
Route::resource('/popular_tag', 'Popular_tagController');
