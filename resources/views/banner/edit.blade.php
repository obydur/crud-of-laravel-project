

@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

        <form action="{{url('/banner/'.$banner->id)}}" method="post" enctype="multipart/form-data">

        @csrf

            {{ method_field('put') }}

        <div class="form-group">
            <label class = "control-label" for="title">Title</label>
            <input id="title" type="text" name="title" value="{{$banner->title}}" placeholder="Write Title Name"  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="picture">Picture</label>
            <input id="picture" type="file" name="picture" value="{{$banner->picture}}" placeholder=""  class="form-control" >
            <input type="image" src="{{asset('images/'.$banner->picture)}}" width="100" height="100">
        </div>

        <div class="form-group">
            <label class = "control-label" for="link">Image Link</label>
            <input id="link" type="text" name="link" value="{{$banner->link}}" placeholder="Please Add image link"  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="promotional_message">Promotional Message</label>
            <textarea name="promotional_message" cols="2" rows="1" type="text" id="promotional_message" class="form-control"></textarea>
        </div>

        <div class="form-group">
            <label class = "control-label" for="html_banner">HTML Banner</label>
            <textarea name="html_banner" id="html_banner" cols="2" rows="1" type="text" class="form-control">          </textarea>
        </div>

        <div class="form-group">
            <label class = "control-label" for="is_active">Is Active</label>
            <input id="is_active" type="number" name="is_active" value="{{$banner->is_active}}" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="is_draft">Is Draft</label>
            <input id="is_draft" type="number" name="is_draft" value="{{$banner->is_draft}}" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="soft_delete">Soft Delete</label>
            <input id="soft_delete" type="number" name="soft_delete" value="{{$banner->soft_delete}}" placeholder=""  class="form-control">
        </div>

        <div class="form-group">
            <label class = "control-label" for="max_display">Max Display</label>
            <input id="max_display" type="number" name="max_display" value="{{$banner->max_display}}" placeholder=""  class="form-control" >
        </div>
        <div class="form-group"><button type="submit" class="btn btn-success">Update</button></div>
    </form>

@endsection

@section('banner')
    n/a
@endsection
