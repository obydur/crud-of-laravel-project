@extends('layoutHome.default')

@section('content')

    <div class="banner">

        <dl>
            <dt>ID</dt>
            <dd>{{$banner->id}}</dd>

            <dt>Title</dt>
            <dd>{{$banner->title}}</dd>

            <dt>Picture</dt>
            <dd> <img src="{{ asset('images/'.$banner->picture) }}" width="100" height="100"></dd>

            <dt>Link</dt>
            <dd>{{$banner->link}}</dd>

            <dt>Promotional Message</dt>
            <dd>{{$banner->promotional_message}}</dd>

            <dt>html_banner</dt>
            <dd>{{$banner->html_banner}}</dd>

            <dt>IS Active</dt>
            <dd>{{$banner->is_active}}</dd>

            <dt>IS Drafte</dt>
            <dd>{{$banner->is_draft}}</dd>

            <dt>Soft Delete</dt>
            <dd>{{$banner->soft_delete}}</dd>

            <dt>Max Display</dt>
            <dd>{{$banner->max_display}}</dd>

            <dt>Created At</dt>
            <dd>{{$banner->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$banner->updated_at}}</dd>
        </dl>

    </div>
@endsection

@section('banner')
    n/a
@endsection
