
@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif



    @php
        $sl = 0;
    @endphp

    <a href="banner/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">
        <thead class="">
        <tr>
            <th>No.</th>
            <th>Title</th>
            <th>picture</th>
            <th>link</th>
            <th>Promotional Message</th>
            <th>HTML Banner</th>
            <th>Is Active</th>
            <th>Is Draft</th>
            <th>Soft Delete</th>
            <th>Max Display</th>
             <th>Action</th>

        </tr>
        </thead>

        @php
            $sl = 0;
        @endphp

        @foreach($banners as $banner)
            <tr>
                <td>{{++$sl}}</td>

                {{--<td><a href="{{url('/form/'.$form->id)}}">{{$form->firstname}}</a> </td>--}}
                <td><a href="{{route('banner.show',['id'=>$banner->id])}}">{{$banner->title}}</a> </td>

                <td><img src="{{ asset('images/'.$banner->picture) }}" width="100" height="100"></td>

                <td>{{$banner->link}}</td>
                <td>{{$banner->promotional_message}}</td>
                <td>{{$banner->html_banner}}</td>
                <td>{{$banner->is_active}}</td>
                <td>{{$banner->is_draft}}</td>
                <td>{{$banner->soft_delete}}</td>
                <td>{{$banner->max_display}}</td>
                    <td><a href="{{route('banner.edit', $banner->id)}}">Edit</a> |
                         {!! Form::open(array('url'=>['banner',$banner->id],'onclick'=>"return confirm('Are you sure you want to delete this data form the data table'); ",'method'=>'DELETE')) !!}
                         <button type="submit" class="btn btn-primary">Delete</button>
                        {!! Form::close() !!}

                    </td>
            </tr>

        @endforeach
    </table>

@endsection

@section('banner')
    n/a
@endsection
