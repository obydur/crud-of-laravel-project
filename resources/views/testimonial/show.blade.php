
@extends('layoutHome.default')

@section('content')

    <div class="testimonial">

        <dl>
            <dt>ID</dt>
            <dd>{{$testimonial->id}}</dd>

            <dt>Picture</dt>
            <dd>{{$testimonial->picture}}</dd>

            <dt>Body</dt>
            <dd>{{$testimonial->body}}</dd>

            <dt>Name</dt>
            <dd>{{$testimonial->name}}</dd>

            <dt>Designation</dt>
            <dd>{{$testimonial->designation}}</dd>

            <dt>Is Active</dt>
            <dd>{{$testimonial->is_active}}</dd>

            <dt>Is Draft</dt>
            <dd>{{$testimonial->is_draft}}</dd>

            <dt>Soft Delete</dt>
            <dd>{{$testimonial->soft_delete}}</dd>

        </dl>

    </div>
@endsection

@section('testimonial')
    n/a
@endsection
