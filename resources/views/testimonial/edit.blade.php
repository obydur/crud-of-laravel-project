
@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


        <form action="{{url('testimonial/'.$testimonial->id)}}" method="post" enctype="multipart/form-data">
        @csrf

            {{method_field('put')}}

        <div class="form-group">
            <label class = "control-label" for="picture">Picture</label>
            <input id="picture" type="file" name="picture" value="{{$testimonial->picture}}" placeholder=""  class="form-control" >

        </div>

        <div class="form-group">
            <label class = "control-label" for="body">Body</label>
            <input id="body" type="text" name="body" value="{{$testimonial->body}}" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="name">Name</label>
            <input id="name" type="text" name="name" value="{{$testimonial->name}}" placeholder=""  class="form-control">
        </div>

        <div class="form-group">
            <label class = "control-label" for="designation">Designation</label>
            <input id="designation" type="text" name="designation" value="{{$testimonial->designation}}" placeholder=""  class="form-control">
        </div>


        <div class="form-group">
            <label class = "control-label" for="is_active">Is Active</label>
            <input id="is_active" type="number" name="is_active" value="{{$testimonial->is_active}}" placeholder=""  class="form-control">
        </div>


        <div class="form-group">
            <label class = "control-label" for="is_draft">Is Draft</label>
            <input id="is_draft" type="number" name="is_draft" value="{{$testimonial->is_draft}}" placeholder=""  class="form-control">
        </div>

        <div class="form-group">
            <label class = "control-label" for="soft_delete">Soft Delete</label>
            <input id="soft_delete" type="number" name="soft_delete" value="{{$testimonial->soft_delete}}" placeholder=""  class="form-control">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Update</button>
        </div>

    </form>

@endsection

@section('testimonial')
    n/a
@endsection
