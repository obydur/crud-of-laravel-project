
@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{url('/testimonial')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label class = "control-label" for="picture">Picture</label>
            <input id="picture" type="file" name="picture" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="body">Body</label>
            <input id="body" type="text" name="body" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="name">Name</label>
            <input id="name" type="text" name="name" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="designation">Designation</label>
            <input id="designation" type="text" name="designation" value="" placeholder=""  class="form-control" >
        </div>


        <div class="form-group">
            <label class = "control-label" for="is_active">Is Active</label>
            <input id="is_active" type="number" name="is_active" value="" placeholder=""  class="form-control" >
        </div>


        <div class="form-group">
            <label class = "control-label" for="is_draft">Is Draft</label>
            <input id="is_draft" type="number" name="is_draft" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="soft_delete">Soft Delete</label>
            <input id="soft_delete" type="number" name="soft_delete" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>

    </form>

@endsection

@section('testimonial')
    n/a
@endsection
