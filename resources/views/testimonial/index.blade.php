{{--
HI AM INDEX PAGE OF CART--}}

@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif



    <a href="testimonial/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">

        <thead class="">
        <tr>
            <th>ID</th>
            <th>Picture</th>
            <th>Body</th>
            <th>Name</th>
            <th>Designation</th>
            <th>Is Active</th>
            <th>Is Draft</th>
            <th>Soft Delete</th>
            <th>Action</th>
        </tr>
        </thead>


        @php
            $sl = 0;
        @endphp


    @foreach($testimonials as $testimonial)

            <tr>
                <td>{{++$sl}}</td>
                <td><img src="{{asset('images/'.$testimonial->picture)}}" width="100"  height="100" alt=""></td>
                <td><a href="{{route('testimonial.show',['id'=>$testimonial->id])}}">{{$testimonial->body}}</a> </td>
                <td><a href="{{route('testimonial.show',['id'=>$testimonial->id])}}">{{$testimonial->name}}</a> </td>
                <td>{{$testimonial->designation}}</td>
                <td>{{$testimonial->is_active}}</td>
                <td>{{$testimonial->is_draft}}</td>
                <td>{{$testimonial->soft_delete}}</td>

                <td>
                    <a href="{{route('testimonial.edit',['id'=>$testimonial->id])}}">Edit </a>|

                    {!! Form::open(array('url' => ['testimonial',$testimonial->id],'onclick'=>"return confirm('Are you sure you want to delete this item?');",'method' => 'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}

                </td>

            </tr>

        @endforeach
    </table>

@endsection

@section('testimonial')
    n/a
@endsection
