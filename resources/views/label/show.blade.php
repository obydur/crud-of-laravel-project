

@extends('layoutHome.default')


@section('content')

    <div class="label">

        <dl>
            <dt>ID</dt>
            <dd>{{$label->id}}</dd>

            <dt>Title</dt>
            <dd>{{$label->title}}</dd>

            <dt>picture</dt>
            <dd>{{$label->picture}}</dd>

        </dl>

    </div>
@endsection

@section('label')
    n/a
@endsection