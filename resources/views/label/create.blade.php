
@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{url('/label')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label class="control-label" for="title">Title</label>
            <input type="text" name="title" placeholder="Title"  class="form-control">
        </div>

        <div class="form-group">
            <label class = "control-label" for="picture">Picture</label>
            <input id="picture" type="file" name="picture" value="" placeholder="Please add your image"  class="form-control" >
        </div>

        <div class="form-group">
            <label>Is Active? </label>
            <input type="radio" name="is_active" value="1">Yes
            <input type="radio" name="is_active" value="0">No
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
    </form

@endsection

@section('label')
    n/a
@endsection
