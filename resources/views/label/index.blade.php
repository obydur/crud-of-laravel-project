

@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <a href="label/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">

        <thead class="">
        <tr>
            <th>SL</th>
            <th>Title</th>
            <th>Picture</th>
            <th>Action</th>

        </tr>
        </thead>

        @php
            $sl = 0;
        @endphp

        @foreach($labels as $label)
            <tr>
                <td>{{$sl++}}</td>
                <td><a href="{{route('label.show',['id'=>$label->id])}}">{{$label->title}}</a> </td>

                <td><img src="{{asset('images/'.$label->picture)}}" width="100" height="100" alt=""></td>
                <td>

                <td>
                    <a href="{{route('label.edit', $label->id)}}">Edit</a> |
                    {!! Form::open(array('url' => ['label',$label->id],'onclick' =>"return confirm('Are you sure you want to delete this data form the data table'); ",'method'=>'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}

                </td>
            </tr>
        @endforeach
    </table>

@endsection

@section('label')
    n/a
@endsection
