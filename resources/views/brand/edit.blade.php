

@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{url('/brand/'.$brand->id)}}" method="post" enctype="multipart/form-data">

        @csrf
        {{ method_field('put') }}

        <div class="form-group">
            <label class = "control-label" for="title">Title</label>
            <input id="title" type="text" name="title" value="{{$brand->title}}" placeholder="write Title Name"  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="is_draft">Is Draft</label>
            <input id="is_draft" type="number" name="is_draft" value="{{$brand->is_draft}}" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="is_active">Is Active</label>
            <input id="is_active" type="number" name="is_active" value="{{$brand->is_active}}" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="soft_delete">Soft Delete</label>
            <input id="soft_delete" type="number" name="soft_delete" value="{{$brand->soft_delete}}" placeholder=""  class="form-control" >
        </div>

        <div class="form-group"><button type="submit" class="btn btn-primary">Add</button></div>
        <div class="form-group"><button type="submit" class="btn btn-primary">Reset</button></div>

    </form>

@endsection

@section('brand')
    n/a
@endsection
