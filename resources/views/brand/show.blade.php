@extends('layoutHome.default')

@section('content')

    <div class="brand">

        <dl>
            <dt>ID</dt>
            <dd>{{$brand->id}}</dd>

            <dt>Title</dt>
            <dd>{{$brand->title}}</dd>

            <dt>IS Drafte</dt>
            <dd>{{$brand->is_draft}}</dd>

            <dt>IS Active</dt>
            <dd>{{$brand->is_active}}</dd>

            <dt>Soft Delete</dt>
            <dd>{{$brand->soft_delete}}</dd>

            <dt>Created At</dt>
            <dd>{{$brand->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$brand->updated_at}}</dd>
        </dl>

    </div>
@endsection

@section('brand')
    n/a
@endsection
