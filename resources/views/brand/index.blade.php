
@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <a href="brand/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">
        <thead class="">
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Is Draft</th>
            <th>Is Active</th>
            <th>Soft Delete</th>
            <th>Action</th>
        </tr>
        </thead>

        @php
            $sl = 0;
        @endphp

    @foreach($brands as $brand)
            <tr>
                <td>{{$sl++}}</td>
                {{--<td><a href="{{url('/form/'.$form->id)}}">{{$form->firstname}}</a> </td>--}}
                <td><a href="{{route('brand.show',['id'=>$brand->id])}}">{{$brand->title}}</a> </td>
                <td>{{$brand->is_draft}}</td>
                <td>{{$brand->is_active}}</td>
                <td>{{$brand->soft_delete}}</td>
                <td>
                    <a href="{{route('brand.edit', $brand->id)}}">Edit</a> |
                    {!! Form::open(array('url' => ['brand',$brand->id],'onclick' =>"return confirm('Are you sure you want to delete this data form the data table'); ",'method'=>'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>

@endsection

@section('brand')
    n/a
@endsection
