
@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{url('/page')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label class="control-label" for="page_title">Page Title</label>
            <input type="text" value="" name="page_title" placeholder="" class="form-control">
        </div>

        <div class="form-group">
            <label class="control-label" for="page_content">Page Content</label>
            <textarea name="page_content" type="text" value="" rows="1" cols="20" placeholder="Your page content..." class="form-control"></textarea>
        </div>

        <div class="form-group">
            <label>Is Active? </label>
            <input type="radio" name="is_active" value="1">Yes
            <input type="radio" name="is_active" value="0">No
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
            <button type="submit" class="btn btn-primary">Reset</button>
            <a href="{{route('page.index',$page->id)}}">Show</a>

        </div>
    </form>


@endsection

@section('page')
    n/a
@endsection
