
@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{url('page/'.$page->id)}}" method="post" enctype="multipart/form-data">
        @csrf

        {{method_field('put')}}


        <div class="form-group">
            <label class="control-label" for="page_title">Page Title</label>
            <input type="text" value="{{$page->page_title}}" name="page_title" placeholder="" class="form-control">
        </div>

        <div class="form-group">
            <label class="control-label" for="page_content">Page Content</label>
            <textarea name="page_content" type="text" value="{{$page->page_content}}" rows="1" cols="20" placeholder="Your page content..." class="form-control"></textarea>
        </div>

        <div class="form-group">
            <label>Is Active? </label>
            <input type="radio" name="is_active" value="1">Yes
            <input type="radio" name="is_active" value="0">No
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
    </form>


@endsection

@section('page')
    n/a
@endsection
