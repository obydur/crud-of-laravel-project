
@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif



    <a href="page/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">
        <thead class="">
        <tr>
            <th>No.</th>
            <th>Page Title</th>
            <th>Page Content</th>
            <th>Action</th>
        </tr>
        </thead>


        @php
            $sl = 0;
        @endphp

        @foreach($pages as $page)
            <tr>
                <td>{{$sl++}}</td>
                <td><a href="{{route('page.show',['id'=>$page->id])}}">{{$page->page_title}}</a> </td>
                <td><a href="{{route('page.show',['id'=>$page->id])}}">{{$page->page_content}}</a> </td>
                <td><a href="{{route('page.edit',$page->id)}}">Edit</a> |

                    {!! Form::open(array('url' => ['page',$page->id],'onclick'=>"return confirm('Are you sure you want to delete this item?');",'method' => 'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}</td>
            </tr>
        @endforeach
    </table>

@endsection
