@extends('layoutHome.default')

@section('content')

    <div class="page">

        <dl>
            <dt>ID</dt>
            <dd>{{$page->id}}</dd>

            <dt>Page Title</dt>
            <dd>{{$page->page_title}}</dd>

            <dt>Page Content</dt>
            <dd>{{$page->page_content}}</dd>

            <dt>Created At</dt>
            <dd>{{$form->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$form->updated_at}}</dd>
        </dl>

    </div>
@endsection

@section('page')
    n/a
@endsection
