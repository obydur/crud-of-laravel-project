{{--
Hi I am created.blade.php file--}}

@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{url('/cart')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label class = "control-label" for="sid">SID</label>
            <input id="sid" type="number" name="sid" value="" placeholder="write product serial Number"  class="form-control">
        </div>

        <div class="form-group">
            <label class = "control-label" for="product_id">Product ID</label>
            <input id="product_id" type="number" name="product_id" value="" placeholder="write product ID "  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="picture">Picture</label>
            <input id="picture" type="file" name="picture" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="product_title">Product Title</label>
            <input id="product_title" type="text" name="product_title" value="" placeholder="Please Add Product Title"  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="qty">QTY</label>
            <input id="qty" type="number" name="qty" value="" placeholder="Write Down your product ITEM"  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="unite_price">Unite Price</label>
            <input id="unite_price" type="number" name="unite_price" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="total_price">Total Price</label>
            <input id="total_price" type="number" name="total_price" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>

    </form>

@endsection

@section('cart')
    n/a
@endsection
