@extends('layoutHome.default')

@section('content')

    <div class="cart">

        <dl>
            <dt>ID</dt>
            <dd>{{$cart->id}}</dd>

            <dt>SID</dt>
            <dd>{{$cart->sid}}</dd>

            <dt>Product ID</dt>
            <dd>{{$cart->product_id}}</dd>

            <dt>Picture</dt>
            <dd>{{$cart->picture}}</dd>

            <dt>Product Title</dt>
            <dd>{{$cart->product_title}}</dd>

            <dt>QTY</dt>
            <dd>{{$cart->qty}}</dd>

            <dt>Unite Price</dt>
            <dd>{{$cart->unite_price}}</dd>

            <dt>Total Price</dt>
            <dd>{{$cart->total_price}}</dd>

        </dl>

    </div>
@endsection


