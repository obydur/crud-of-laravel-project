{{--
Hi I am edit.blade.php file--}}

@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{url('/cart/'.$cart->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('put') }}

        <div class="form-group">
            <label class = "control-label" for="sid">SID</label>
            <input id="sid" type="number" name="sid" value="{{$cart->id}}" placeholder=""  class="form-control" required>
        </div>

        <div class="form-group">
            <label class = "control-label" for="product_id">Product ID</label>
            <input id="product_id" type="number" name="product_id" value="{{$cart->id}}" placeholder=" "  class="form-control" required>
        </div>

        <div class="form-group">
           <label class = "control-label" for="picture">Picture</label>

            <img src="{{ asset('/picture/'.$cart->picture) }}" width="150"><br><br>

            <input id="picture" type="file" name="picture" value="" placeholder=""  class="" required>
        </div>


        <div class="form-group">
            <label class = "control-label" for="product_title">Product Title</label>
            <input id="product_title" type="text" name="product_title" value="{{$cart->id}}" placeholder=""  class="form-control" required>
        </div>


        <div class="form-group">
            <label class = "control-label" for="qty">QTY</label>
            <input id="qty" type="number" name="qty" value="{{$cart->id}}" placeholder=""  class="form-control" required>
        </div>


        <div class="form-group">
            <label class = "control-label" for="unite_price">Unite Price</label>
            <input id="unite_price" type="number" name="unite_price" value="{{$cart->id}}" placeholder=""  class="form-control" required>
        </div>

        <div class="form-group">
            <label class = "control-label" for="total_price">Total Price</label>
            <input id="total_price" type="number" name="total_price" value="{{$cart->id}}" placeholder=""  class="form-control" required>
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>

    </form>

@endsection

@section('cart')
    n/a
@endsection
