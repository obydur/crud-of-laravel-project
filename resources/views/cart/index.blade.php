{{--
HI AM INDEX PAGE OF CART--}}

@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    

    <a href="cart/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">
        <thead class="">
        <tr>
            <th>ID</th>
            <th>SID</th>
            <th>Product Id</th>
            <th>Picture</th>
            <th>Product Title</th>
            <th>Qty</th>
            <th>Unite Price</th>
            <th>Total Price</th>
            <th>Action</th>
        </tr>
        </thead>

        @php
            $sl = 1;
        @endphp
        
        @foreach($carts as $cart)
            <tr>
                <td>{{$sl++}}</td>
                {{--<td><a href="{{url('/form/'.$form->id)}}">{{$form->firstname}}</a> </td>--}}
                <td>{{$cart->sid}}</td>
                <td>{{$cart->product_id}}</td>
                <td><a href="{{route('cart.show',['id'=>$cart->id])}}"><img src="{{asset('images/'.$cart->picture)}}" width="100" height="100" alt=""></a></td>
                <td>{{$cart->product_title}}</td>
                <td>{{$cart->qty}}</td>
                <td>{{$cart->unite_price}}</td>
                <td>{{$cart->total_price}}</td>
                <td>
                    <a href="{{route('cart.edit', $cart->id)}}">Edit</a> |
                    {!! Form::open(array('url' => ['cart',$cart->id],'onclick' =>"return confirm('Are you sure you want to delete this data form the data table'); ",'method'=>'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}

                </td>
            </tr>
        @endforeach
    </table>

@endsection

@section('cart')
    n/a
@endsection
