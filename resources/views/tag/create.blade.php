
@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">

                <h3 class="card-title text-center"> Create Form </h3>
                <form action="{{url('/tag')}}" method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label class = "control-label" for="title"><b>TITLE</b></label>
                            <input id="title" type="text" name="title" value="" placeholder="write title name"  class="form-control" required>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="submit" class="btn btn-primary">Reset</button>
                        </div>

                </form>

            </div>
        </div>
    </div>

@endsection

@section('tag')
    n/a
@endsection

