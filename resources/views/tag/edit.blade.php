@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{url('tag/'.$tag->id)}}" method="post" enctype="multipart/form-data">
        @csrf


        {{ method_field('put') }}

        <div class="form-group">
            <label class="control-label" for="title">Title</label>
            <input type="text" value="{{$tag->title}}" name="title"  class="form-control">
        </div>

        <div class="form-group">
            <label>Is Active? </label>
            @if($tag->is_active==1)
                <input type="radio" name="is_active" value="1" checked>Yes
                <input type="radio" name="is_active" value="0">No
            @else
                <input type="radio" name="is_active" value="1">Yes
                <input type="radio" name="is_active" value="0" checked>No
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Update</button>
        </div>
    </form>


@endsection

