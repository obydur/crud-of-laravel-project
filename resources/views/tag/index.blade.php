@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <a href="tag/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">

        <thead class="">
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Action</th>

        </tr>
        </thead>

        @php
            $sl = 1;
        @endphp

        @foreach($tags as $tag)
            <tr>
                <td>{{$sl++}}</td>
                <td><a href="{{url('/tag/'.$tag->id)}}">{{$tag->title}}</a> </td>

                <td>
                    <a href="{{route('tag.edit',['id'=>$tag->id])}}">Edit </a> |
                    {!! Form::open(array('url' => ['tag',$tag->id],'onclick'=>"return confirm('Are you sure you want to delete this item?');",'method' => 'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
    </table>

@endsection

@section('tag')
    n/a
@endsection





