@extends('layoutHome.default')


@section('content')

    <div class="card">

        <dl>
            <dt>ID</dt>
            <dd>{{$tag->id}}</dd>

            <dt>Title</dt>
            <dd>{{$tag->title}}</dd>

            <dt>Created At</dt>
            <dd>{{$tag->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$tag->updated_at}}</dd>
        </dl>

    </div>
@endsection

@section('tag')
    n/a
@endsection
