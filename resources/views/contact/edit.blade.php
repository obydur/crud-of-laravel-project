

@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{url('contact/'.$contact->id)}}" method="post" enctype="multipart/form-data">

        @csrf

        {{method_field('put')}}

        <div class="form-group">
            <label class = "control-label" for="name">Name</label>
            <input id="name" type="text" name="name" value="{{$contact->name}}" placeholder="write your name"  class="form-control">
        </div>

        <div class="form-group">
            <label class = "control-label" for="email">Email ID</label>
            <input id="email" type="text" name="email" value="{{$contact->email}}" placeholder="write  your email ID "  class="form-control">
        </div>

        <div class="form-group">
            <label class = "control-label" for="phone_no">Phone No</label>
            <input id="phone_no" type="number" name="phone_no" value="{{$contact->phone_no}}" placeholder="write  your phone no "  class="form-control">
        </div>

        <div class="form-group">
            <label class = "control-label" for="subject">Subject</label>
            <input id="subject" type="text" name="subject" value="{{$contact->subject}}" placeholder="Please add your subject"  class="form-control">
        </div>

        <div class="form-group">
            <label class = "control-label" for="comment">Comment</label>
            <textarea name="comment" type="text" value="{{$contact->comment}}" rows="1" cols="20" placeholder="Your product comment..." class="form-control"></textarea>
        </div>

        <div class="form-group">
            <label class = "control-label" for="status">Status</label>
            <textarea name="status" type="text" value="{{$contact->status}}" rows="1" cols="20" placeholder="Your product comment..." class="form-control" required></textarea>

        </div>

        <div class="form-group">
            <label class = "control-label" for="soft_delete">Soft Delete</label>
            <input id="soft_delete" type="number" name="soft_delete" value="{{$contact->soft_delete}}" placeholder=""  class="form-control">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
            <button type="submit" class="btn btn-primary">Reset</button>
        </div>

    </form>

@endsection

@section('contact')
    n/a
@endsection
