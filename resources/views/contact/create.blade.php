

@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{url('/contact')}}" method="post" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label class = "control-label" for="name">Name</label>
            <input id="name" type="text" name="name" value="" placeholder="write your name"  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="email">Email ID</label>
            <input id="email" type="text" name="email" value="" placeholder="write  your email ID "  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="phone_no">Phone NO</label>
            <input id="phone_no" type="number" name="phone_no" value="" placeholder="write  your email ID "  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="subject">Subject</label>
            <input id="subject" type="text" name="subject" value="" placeholder="Please add your subject"  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="comment">Comment</label>
            <textarea name="comment" type="text"  rows="1" cols="20" placeholder="Your product comment..." class="form-control">value=""</textarea>
        </div>

        <div class="form-group">
            <label class = "control-label" for="status">Status</label>
            <textarea name="status" type="text"  rows="1" cols="20" placeholder="Your product comment..." class="form-control" >value=""</textarea>

        </div>

        <div class="form-group">
            <label class = "control-label" for="soft_delete">Soft Delete</label>
            <input id="soft_delete" type="number" name="soft_delete" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>

    </form>

@endsection

@section('contact')
    n/a
@endsection
