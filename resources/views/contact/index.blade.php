

@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <a href="contact/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">

        <thead class="">
        <tr>
            <th>ID</th>
            <th>name</th>
            <th>Email</th>
            <th>Phone NO</th>
            <th>Subject</th>
            <th>Comment</th>
            <th>Status</th>
            <th>Soft Delete</th>
            <th>Action</th>

        </tr>
        </thead>

        @php
            $sl = 0;
        @endphp

        @foreach($contacts as $contact)
            <tr>
                <td>{{$sl++}}</td>
                {{--<td><a href="{{url('/form/'.$form->id)}}">{{$form->firstname}}</a> </td>--}}
                <td><a href="{{route('contact.show',['id'=>$contact->id])}}">{{$contact->name}}</a> </td>
                <td>{{$contact->email}}</td>
                <td>{{$contact->phone_no}}</td>
                <td><a href="{{route('contact.show',['id'=>$contact->id])}}">{{$contact->subject}}</a></td>
                <td>{{$contact->comment}}</td>
                <td>{{$contact->status}}</td>
                <td>{{$contact->soft_delete}}</td>
                <td>
                    <a href="{{route('contact.edit', $contact->id)}}">Edit</a> |
                    {!! Form::open(array('url' => ['contact',$contact->id],'onclick' =>"return confirm('Are you sure you want to delete this data form the data table'); ",'method'=>'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}
                </td>

            </tr>
        @endforeach
    </table>

@endsection

@section('contact')
    n/a
@endsection
