
@extends('layoutHome.default')

@section('content')

    <div class="contact">

        <dl>
            <dt>ID</dt>
            <dd>{{$contact->id}}</dd>

            <dt>Name</dt>
            <dd>{{$contact->name}}</dd>

            <dt>Email</dt>
            <dd>{{$contact->email}}</dd>

            <dt>Phone NO</dt>
            <dd>{{$contact->phone_no}}</dd>

            <dt>Subject</dt>
            <dd>{{$contact->subject}}</dd>

            <dt>Comment</dt>
            <dd>{{$contact->comment}}</dd>

            <dt>Status</dt>
            <dd>{{$contact->status}}</dd>

            <dt>Soft Delete</dt>
            <dd>{{$contact->soft_delete}}</dd>

            <dt>Created At</dt>
            <dd>{{$contact->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$contact->updated_at}}</dd>

        </dl>

    </div>
@endsection

@section('contact')
    n/a
@endsection
