
@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">

                <h3 class="card-title text-center"> Create Form </h3>
                <form action="{{url('/product')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class = "control-label" for="category_id">Category ID</label>
                        <input id="category_id" type="number" name="category_id" value="" placeholder="write Title Name"  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="product_title">Product Title</label>
                        <input id="product_title" type="text" name="product_title" value="" placeholder="write product Title Name"  class="form-control" >
                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="picture">Picture</label>
                        <input id="picture" type="file" name="picture" value="" placeholder=""  class="form-control" >
                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="short_description">Short Description</label>
                        <textarea type="text" value="" name="short_description" rows="1" cols="20" placeholder="Your short  description..." id="short_description"  class="form-control" ></textarea>

                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="description">Description</label>

                        <textarea type="text" value="" name="description" rows="1" cols="20" placeholder="Your description..." id="description"  class="form-control"></textarea>

                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="promotional_message">Promotional Message</label>
                        <textarea type="text" value="" name="promotional_message" rows="1" cols="20" placeholder="Your Reason..." id="promotional_message"  class="form-control"></textarea>

                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="additional_information">Additional Information</label>
                        <textarea type="text" value="" name="additional_information" rows="1" cols="20" placeholder="Your Reason..." id="additional_information"  class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="price">Price</label>
                        <input id="price" type="number" name="price" value="" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="special_price">Special Price</label>
                        <input id="special_price" type="number" name="special_price" value="" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="offer">Offer</label>
                        <input id="Offer" type="number" name="offer" value="" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="start_date">Start Date</label>
                        <input id="start_date" type="date" name="start_date" value="" placeholder=""  class="form-control" >
                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="end_date">End Date</label>
                        <input id="end_date" type="date" name="end_date" value="" placeholder=""  class="form-control" >
                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="sku">SKU</label>
                        <input id="sku" type="text" name="sku" value="" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="product_url">Product URL</label>
                        <input id="product_url" type="text" name="product_url" value="" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="created_by">Created By</label>
                        <input id="created_by" type="text" name="created_by" value="" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="updated_by">Updated By</label>
                        <input id="updated_by" type="text" name="updated_by" value="" placeholder=""  class="form-control" >
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Add</button>
                        <button type="submit" class="btn btn-primary">Reset</button>
                    </div>
                </form>
            </div>
        </div>



        @endsection

        @section('product')
            n/a
@endsection
