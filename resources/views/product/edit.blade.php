
@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">

                <h3 class="card-title text-center"> Create Form </h3>

                    <form action="{{url('product/'.$product->id)}}" method="post" enctype="multipart/form-data">

                    @csrf

                    {{ method_field('put') }}

                    <div class="form-group">
                        <label class = "control-label" for="category_id">Category ID</label>
                        <input id="category_id" type="number" name="category_id" value="{{$product->category_id}}" placeholder="write Title Name"  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="product_title">Product Title</label>
                        <input id="product_title" type="text" name="product_title" value="{{$product->product_title}}" placeholder="write product Title Name"  class="form-control" required>
                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="picture">Picture</label>
                        <input id="picture" type="file" name="picture" value="{{$product->picture}}" placeholder=""  class="form-control" required>
                        <input type="image" src="{{ asset('images/'.$product->picture) }}" width="100" height="100">
                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="short_description">Short Description</label>
                        <textarea type="text" name="short_description" rows="1" cols="20"  id="short_description"  class="form-control" > {{$product->short_description}}</textarea>


                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="description">Description</label>
                        <textarea type="text" value="{{$product->description}}" name="description" rows="1" cols="20" placeholder="Your description..." id="description"  class="form-control"></textarea>

                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="promotional_message">Promotional Message</label>
                        <textarea type="text" value="{{$product->promotional_message}}" name="promotional_message" rows="1" cols="20" placeholder="Your Reason..." id="promotional_message"  class="form-control"></textarea>

                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="additional_information">Additional Information</label>
                        <textarea type="text" value="{{$product->additional_information}}" name="additional_information" rows="1" cols="20" placeholder="Your Reason..." id="additional_information"  class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="price">Price</label>
                        <input id="price" type="number" name="price" value="{{$product->price}}" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="special_price">Special Price</label>
                        <input id="special_price" type="number" name="special_price" value="{{$product->special_price}}}" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="offer">Offer</label>
                        <input id="Offer" type="number" name="offer" value="{{$product->offer}}" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="start_date">Start Date</label>
                        <input id="start_date" type="date" name="start_date" value="{{$product->start_date}}" placeholder=""  class="form-control" >
                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="end_date">End Date</label>
                        <input id="end_date" type="date" name="end_date" value="{{$product->end_date}}" placeholder=""  class="form-control" >
                    </div>


                    <div class="form-group">
                        <label class = "control-label" for="sku">SKU</label>
                        <input id="sku" type="text" name="sku" value="{{$product->sku}}" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="product_url">Product URL</label>
                        <input id="product_url" type="text" name="product_url" value="{{$product->product_url}}" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="created_by">Created By</label>
                        <input id="created_by" type="text" name="created_by" value="{{$product->created_by}}" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="updated_by">Updated By</label>
                        <input id="updated_by" type="text" name="updated_by" value="{{$product->updated_by}}" placeholder=""  class="form-control" >
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Add</button>
                        <button type="submit" class="btn btn-primary">Reset</button>
                    </div>
                </form>
            </div>
        </div>



        @endsection
        @section('product')
            n/a
@endsection
