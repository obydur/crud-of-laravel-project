@extends('layoutHome.default')

@section('content')

    <div class="card">

        <dl>
            <dt>ID</dt>
            <dd>{{$product->id}}</dd>

            <dt>Category ID</dt>
            <dd>{{$product->category_id}}</dd>

            <dt>Product Title</dt>
            <dd>{{$product->product_title}}</dd>

            <dt>Picture</dt>
            <dd> <img src="{{ asset('images/'.$product->picture) }}" width="100" height="100"></dd>

            <dt>Short Description</dt>
            <dd>{{$product->short_description}}</dd>

            <dt>Description</dt>
            <dd>{{$product->description}}</dd>

            <dt>additional_information</dt>
            <dd>{{$product->additional_information}}</dd>

            <dt>Price</dt>
            <dd>{{$product->price}}</dd>

            <dt>Special Price</dt>
            <dd>{{$product->special_price}}</dd>

            <dt>Offer</dt>
            <dd>{{$product->offer}}</dd>

            <dt>Start Date</dt>
            <dd>{{$product->start_date}}</dd>

            <dt>End Date</dt>
            <dd>{{$product->end_date}}</dd>

            <dt>SKU</dt>
            <dd>{{$product->sku}}</dd>

            <dt>Product Url</dt>
            <dd>{{$product->product_url}}</dd>

            <dt>Created By</dt>
            <dd>{{$product->created_by}}</dd>

            <dt>updated_by</dt>
            <dd>{{$product->updated_by}}</dd>

            <dt>Created At</dt>
            <dd>{{$product->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$product->updated_at}}</dd>
        </dl>

    </div>
@endsection

@section('product')
    n/a
@endsection
