
@extends('layoutHome.default')

@section('content')

    <div class="row">

        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title text-center"> Index Table </h4>
                    <a class="btn btn-success btn-sm easyAccess" href="{{route('product.create')}}"> Again Add<i class="mdi mdi-playlist-plus"></i></a>

                    @if(session('message'))
                        <div class="alert alert-success"> {{session('message')}} </div>
                    @endif

                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger"> {{$error}} </div>
                        @endforeach
                    @endif

                    <div class="table-responsive">
                    <a href="product/create" class="btn btn-outline-info">Add New</a>
                        <table class="table table-striped table-bordered table-hover">
                            <thead class="">
                            <tr>
                                <th> ID </th>
                                <th> Category ID </th>
                                <th> Product Title </th>
                                <th> Picture </th>
                                <th> Short_description</th>
                                <th> Description </th>
                                <th> Additional Information </th>
                                <th> Price </th>
                                <th> Special_price </th>
                                <th> Offer </th>
                                <th> Start Date</th>
                                <th> End Date</th>
                                <th> SKU</th>
                                <th> Product URL</th>
                                <th> Created By</th>
                                <th> Updated By</th>
                                <th> Actions </th>
                            </tr>
                            </thead>
                            <tbody>

                            @php
                                $sl = 1;
                            @endphp

                            @foreach($products as $product)
                                <tr>
                                    <td>{{$sl++}}</td>
                                    <td> {{$product->category_id}} </td>
                                    <td> {{$product->product_title}} </td>

                                    <td><img src="{{ asset('images/'.$product->picture) }}" width="100" height="100"></td>

                                    {{-- <td>
                                        @if(!empty($product->picture))
                                            <img src="{{asset("/images/products/".$product->picture)}}" width="150px" height="150px" alt="Not Found"> </td>
                                    @else
                                        <strong> {{"Empty Image"}} </strong>
                                    @endif--}}

                                    <td> {{$product->short_description}}</td>
                                    <td> {{$product->description}}</td>
                                    <td> {{$product->additional_information}}</td>
                                    <td> {{$product->price}}</td>
                                    <td> {{$product->special_price}} </td>
                                    <td> {{$product->offer}} </td>
                                    <td> {{$product->start_date}} </td>
                                    <td> {{$product->end_date}} </td>
                                    <td> {{$product->sku}} </td>
                                    <td> {{$product->product_url}} </td>
                                    <td> {{$product->created_by}} </td>
                                    <td> {{$product->updated_by}} </td>



                                    <td><a href="{{route('product.edit',['id'=>$product->id])}}">Edit |

                                            {!! Form::open(array('url'=>['product',$product->id],'onclick'=>"return confirm ('Are you sure you want to delete this item?');",'method'=>'DELETE')) !!}
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                        {!! Form::close() !!}

                                    </td>

                                   {{-- <td class="actionButton">
                                        <a class="btn btn-success btn-icon" href="{{route('product.show', $product->id)}}"> <i class="mdi mdi-eye"></i> </a>
                                        <a class="btn btn-primary btn-icon" href="{{route('product.edit', $product->id)}}"> <i class="mdi mdi-pencil"></i> </a>
                                        <a class="btn btn-info btn-icon" href="{{route('product.show', $product->id)}}"> <i class="mdi mdi-eye-off"></i> </a>

                                        {!! Form::open(array('url' => route('product.destroy', $product->id),'method' => 'delete', 'class' => 'deleteForm')) !!}
                                        {{ Form::button('<i class="mdi mdi-delete-forever"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-icon'] )  }}
                                        {!! Form::close() !!}
                                    </td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection

@section('product')
    n/a
@endsection
