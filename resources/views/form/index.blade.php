
@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif



    <a href="form/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">
        <thead class="">
        <tr>
            <th>No.</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Password</th>
            <th>Date of Birth</th>
            <th>Skill</th>
            <th>Gender</th>
            <th>Car</th>
            <th>Message</th>
            <th>Action</th>

        </tr>
        </thead>

        @php
            $sl = 0;
        @endphp

        @foreach($forms as $form)
            <tr>
                <td>{{$sl++}}</td>
                {{--<td><a href="{{url('/form/'.$form->id)}}">{{$form->firstname}}</a> </td>--}}
                <td><a href="{{route('form.show',['id'=>$form->id])}}">{{$form->firstname}}</a> </td>
                <td>{{$form->lastname}}</td>
                <td>{{$form->password}}</td>
                <td>{{$form->date_of_birth}}</td>
                <td>{{$form->skill}}</td>
                <td>{{$form->gender}}</td>
                <td>{{$form->car}}</td>
                <td>{{$form->message}}</td>
                <td><a href="#">Edit</a> | <a href="#">Delete</a></td>
            </tr>
        @endforeach
    </table>

@endsection
