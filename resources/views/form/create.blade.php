
@extends('layoutHome.default')

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



        <form action="{{url('/form')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class = "control-label" for="first-name">First Name</label>
                <input id="first-name" type="text" name="first-name" value="" placeholder="write your First Name"  class="form-control" required>
            </div>

            <div class="form-group">
                <label class = "control-label" for="last-name">First Name</label>
                <input id="last-name" type="text" name="last-name" value="" placeholder="write your First Name"  class="form-control" required>
            </div>


            <div class="form-group">
                <label class = "control-label" for="password">Password</label>
                <input id="password" type="password" name="password" value="" placeholder="write your password"  class="form-control" required>
            </div>

            <div class="form-group">
                <label class = "control-label" for="date_of_birth">Date of birth</label>
                <input id="date_of_birth" type="date" name="date_of_birth" value="" placeholder=""  class="form-control" required>
            </div>
            <p> <h6>Please Click your Skill:</h6> </p>
            <div class="form-group">
                <label class = "control-label" for="programming">
                    <input id="programming" type="checkbox" name="skill" value="programming" placeholder=""  class="form-control" >Programming:</label>

                <label class = "control-label" for="graphic-design">
                    <input id="graphic-design" type="checkbox" name="skill" value="graphic-design" placeholder=""  class="form-control" >Graphic Design:</label>

                <label class = "control-label" for="web-design">
                    <input id="web-design" type="checkbox" name="skill" value="web-design" placeholder=""  class="form-control" >Web Design:</label>
            </div>

            <p><h6>Please Type your Gender:</h6></p>

            <div class="form-group">
                <label class = "control-label" for="male">
                    <input id="male" type="radio" name="gender" value="male" placeholder=""  class="form-control" >Male:</label>

                <label class = "control-label" for="female">
                    <input id="female" type="radio" name="gender" value="female" placeholder=""  class="form-control" >Female:</label>
            </div>

            <div class="form-group">

                <label><h6>Select Your Car Brand Name: </h6></label>
                <select name="car-brand">
                    <option value="volvo">Volvo</option>
                    <option value="marcedes">Marcedes</option>
                    <option value="audi">Audi</option>
                </select>
            </div>

            <div>
                <label><h6>Type your own Message:</h6></label>
                <div>
                    <textarea rows="2" cols="15" name="massage"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label><h6>Is It Active or Not? </h6></label>
                <input type="radio" name="is_active" value="1">Yes
                <input type="radio" name="is_active" value="0">No
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>


        </form>

@endsection

@section('form')
    n/a
@endsection
