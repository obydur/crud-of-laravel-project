@extends('layoutHome.default')

@section('content')

    <div class="form">

        <dl>
            <dt>ID</dt>
            <dd>{{$form->id}}</dd>

            <dt>First Name</dt>
            <dd>{{$form->firstname}}</dd>

            <dt>Last Name</dt>
            <dd>{{$form->lastname}}</dd>

            <dt>Password</dt>
            <dd>{{$form->password}}</dd>

            <dt>Date Of Birth</dt>
            <dd>{{$form->date_of_birth}}</dd>

            <dt>Skill</dt>
            <dd>{{$form->skill}}</dd>

            <dt>Gender</dt>
            <dd>{{$form->gender}}</dd>

            <dt>Car</dt>
            <dd>{{$form->car}}</dd>

            <dt>Message</dt>
            <dd>{{$form->message}}</dd>

            <dt>Created At</dt>
            <dd>{{$form->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$form->updated_at}}</dd>
        </dl>

    </div>
@endsection

@section('form')
    n/a
@endsection
