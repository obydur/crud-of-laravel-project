
@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title text-center"> Create Form </h3>
                <form action="{{url('sponser/'.$sponser->id)}}" method="post" enctype="multipart/form-data">

                    @csrf

                    {{ method_field('put') }}

                    <div class="form-group">
                        <label class = "control-label" for="title">Title</label>
                        <input id="title" type="text" name="title" value="{{$sponser->title}}" placeholder="write Title Name"  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="picture">Picture</label>
                        <img src="{{ asset('/picture/'.$sponser->picture) }}" width="150"><br><br>
                        <input id="picture" type="file" name="picture" value="{{$sponser->picture}}" placeholder=""  class="" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="link">Image Link</label>
                        <input id="link" type="text" name="link" value="{{$sponser->link}}" placeholder="Please Add image link"  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="promotional_message">Promotional Message</label>
                        <input id="promotional_message" type="text" name="promotional_message" value="{{$sponser->promotional_message}}" placeholder="Write Down your promotional message"  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="html_banner">HTML Banner</label>
                        <input id="html_banner" type="text" name="html_banner" value="{{$sponser->html_banner}}" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="is_active">Is Active</label>
                        <input id="is_active" type="number" name="is_active" value="{{$sponser->is_active}}" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="is_draft">Is Draft</label>
                        <input id="is_draft" type="number" name="is_draft" value="{{$sponser->is_draft}}" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <label class = "control-label" for="soft_delete">Soft Delete</label>
                        <input id="soft_delete" type="number" name="soft_delete" value="{{$sponser->soft_delete}}" placeholder=""  class="form-control" >
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
            </div>
        </div>
        </form>


        @endsection

        @section('sponser')
            n/a
@endsection
