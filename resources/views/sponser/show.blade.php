@extends('layoutHome.default')

@section('content')

    <div class="card">

        <dl>
            <dt>ID</dt>
            <dd>{{$sponser->id}}</dd>

            <dt>Title</dt>
            <dd>{{$sponser->title}}</dd>

            <dt>Picture</dt>
            <dd> <img src="{{ asset('images/'.$sponser->picture) }}" width="100" height="100"></dd>

            <dt>Link</dt>
            <dd>{{$sponser->link}}</dd>

            <dt>Promotional Message</dt>
            <dd>{{$sponser->promotional_message}}</dd>

            <dt>html_banner</dt>
            <dd>{{$sponser->html_banner}}</dd>

            <dt>IS Active</dt>
            <dd>{{$sponser->is_active}}</dd>

            <dt>IS Drafte</dt>
            <dd>{{$sponser->is_draft}}</dd>

            <dt>Soft Delete</dt>
            <dd>{{$banner->soft_delete}}</dd>

            <dt>Created At</dt>
            <dd>{{$sponser->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$sponser->updated_at}}</dd>
        </dl>

    </div>
@endsection

@section('sponser')
    n/a
@endsection
