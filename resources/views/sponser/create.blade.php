
@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">

                <h3 class="card-title text-center"> Create Form </h3>
                <form action="{{url('/sponser')}}" method="post" enctype="multipart/form-data">
                    @csrf
        <div class="form-group">
            <label class = "control-label" for="title">Title</label>
            <input id="title" type="text" name="title" value="" placeholder="write Title Name"  class="form-control" >
        </div>


        <div class="form-group">
            <label class = "control-label" for="picture">Picture</label>
            <input id="picture" type="file" name="picture" value="" placeholder=""  class="form-control" >
        </div>


        <div class="form-group">
            <label class = "control-label" for="link">Image Link</label>
            <input id="link" type="text" name="link" value="" placeholder="Please Add image link"  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="promotional_message">Promotional Message</label>
            <input id="promotional_message" type="text" name="promotional_message" value="" placeholder="Write Down your promotional message"  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="html_banner">HTML Banner</label>
            <input id="html_banner" type="text" name="html_banner" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="is_active">Is Active</label>
            <input id="is_active" type="number" name="is_active" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="is_draft">Is Draft</label>
            <input id="is_draft" type="number" name="is_draft" value="" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="soft_delete">Soft Delete</label>
            <input id="soft_delete" type="number" name="soft_delete" value="" placeholder=""  class="form-control" >
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>

                </form>
        </div>
        </div>



@endsection

@section('sponser')
    n/a
@endsection
