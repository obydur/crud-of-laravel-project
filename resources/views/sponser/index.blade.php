
@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif



    @php
        $sl = 0;
    @endphp

    <a href="sponser/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">
        <thead class="">

        <tr>
            <th>No.</th>
            <th>Title</th>
            <th>picture</th>
            <th>link</th>
            <th>Promotional Message</th>
            <th>HTML Banner</th>
            <th>Is Active</th>
            <th>Is Draft</th>
            <th>Soft Delete</th>
             <th>Action</th>

        </tr>
        </thead>

        @php
            $sl = 0;
        @endphp

              @foreach($sponsers as $sponser)
            <tr>
                <td>{{++$sl}}</td>

                {{--<td><a href="{{url('/form/'.$form->id)}}">{{$form->firstname}}</a> </td>--}}
                <td><a href="{{route('sponser.show',['id'=>$sponser->id])}}">{{$sponser->title}}</a> </td>

                <td><img src="{{ asset('images/'.$sponser->picture) }}" width="100" height="100"></td>

                <td>{{$sponser->link}}</td>
                <td>{{$sponser->promotional_message}}</td>
                <td>{{$sponser->html_banner}}</td>
                <td>{{$sponser->is_active}}</td>
                <td>{{$sponser->is_draft}}</td>
                <td>{{$sponser->soft_delete}}</td>
                <td><a href="{{route('sponser.edit',['id'=>$sponser->id])}}">Edit |

                    {!! Form::open(array('url'=>['sponser',$sponser->id],'onclick'=>"return confirm ('Are you sure you want to delete this item?');",'method'=>'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}

                </td>
            </tr>
        @endforeach
    </table>

@endsection

@section('sponser')
    n/a
@endsection
