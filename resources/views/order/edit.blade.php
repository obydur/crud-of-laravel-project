
@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{url('order/'.$order->id)}}" method="post" enctype="multipart/form-data">

        @csrf

        {{method_field('put')}}


        <div class="form-group">
            <label class = "control-label" for="product_id">Product Id</label>
            <input id="product_id" type="number" name="product_id" value="{{$order->product_id}}" placeholder=""  class="form-control" required>
        </div>

        <div class="form-group">
            <label class = "control-label" for="qty">Quantity</label>
            <input id="qty" type="number" name="qty" value="{{$order->qty}}" placeholder=""  class="form-control" required>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>

    </form>

@endsection

@section('contact')
    n/a
@endsection
