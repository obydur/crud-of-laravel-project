@extends('layoutHome.default')

@section('content')

    <div class="order">

        <dl>
            <dt>ID</dt>
            <dd>{{$order->id}}</dd>

            <dt>Product ID</dt>
            <dd>{{$order->product_id}}</dd>

            <dt>Quantity</dt>
            <dd>{{$order->qty}}</dd>

            <dt>Created At</dt>
            <dd>{{$order->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$order->updated_at}}</dd>
        </dl>

    </div>
@endsection

@section('order')
    n/a
@endsection
