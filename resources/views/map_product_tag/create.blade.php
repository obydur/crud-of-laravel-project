{{--
Hi I am created.blade.php file--}}

@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


     <form action="{{url('/map_product_tag')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label class = "control-label" for="product_id">Product Id</label>
            <input id="product_id" type="number" name="product_id" value="" placeholder=""  class="form-control" required>
        </div>

        <div class="form-group">
            <label class = "control-label" for="tag_id">Tag ID</label>
            <input id="tag_id" type="number" name="tag_id" value="" placeholder=""  class="form-control" required>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
            <button type="submit" class="btn btn-primary">Reset</button>
        </div>

    </form>

@endsection

@section('contact')
    n/a
@endsection
