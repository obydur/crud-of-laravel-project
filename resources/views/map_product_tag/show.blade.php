@extends('layoutHome.default')

@section('content')

    <div class="card">

        <dl>
            <dt>ID</dt>
            <dd>{{$map_product_tag->id}}</dd>

            <dt>Product ID</dt>
            <dd>{{$map_product_tag->product_id}}</dd>

            <dt>Tag Id</dt>
            <dd>{{$map_product_tag->tag_id}}</dd>

            <dt>Created At</dt>
            <dd>{{$map_product_tag->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$map_product_tag->updated_at}}</dd>
        </dl>

    </div>
@endsection

@section('map_product_tag')
    n/a
@endsection
