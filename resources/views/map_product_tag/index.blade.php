
@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <a href="map_product_tag/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">

        <thead class="">
        <tr>
            <th>ID</th>
            <th>ProductId</th>
            <th>Tag Id</th>
            <th>Action</th>

        </tr>
        </thead>

        @php
            $sl = 0;
        @endphp

        @foreach($map_product_tags as $map_product_tag)
            <tr>
                <td>{{$sl++}}</td>
                <td><a href="{{route('map_product_tag.show',['id'=>$map_product_tag->id])}}">{{$map_product_tag->product_id}}</a> </td>
                <td>{{$map_product_tag->tag_id}}</td>
                <td>
                    <a href="{{route('map_product_tag.edit', $map_product_tag->id)}}">Edit</a> |
                {!! Form::open(array('url' => ['map_product_tag',$map_product_tag->id],'onclick' =>"return confirm('Are you sure you want to delete this data form the data table'); ",'method'=>'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}
                </td>

            </tr>

        @endforeach
    </table>

@endsection

@section('map_product_tag')
    n/a
@endsection
