

@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <a href="popular_tag/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">

        <thead class="">
        <tr>
            <th>ID</th>
            <th>name</th>
            <th>Link</th>
            <th>Soft Delete</th>
            <th>Is Draft</th>
            <th>Action</th>

        </tr>
        </thead>

        @php
            $sl = 9;
        @endphp

        @foreach($popular_tags as $popular_tag)
            <tr>
                <td>{{++$sl}}</td>

                <td><a href="{{route('popular_tag.show',['id'=>$popular_tag->id])}}">{{$popular_tag->name}}</a> </td>
                <td><a href="{{route('popular_tag.show',['id'=>$popular_tag->id])}}">{{$popular_tag->link}}</a> </td>
                <td>{{$popular_tag->soft_delete}}</td>
                <td>{{$popular_tag->is_draft}}</td>
                <td>
                    <a href="{{route('popular_tag.edit', $popular_tag->id)}}">Edit</a> |
                    {!! Form::open(array('url' => ['popular_tag',$popular_tag->id],'onclick' =>"return confirm('Are you sure you want to delete this data form the data table'); ",'method'=>'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>

@endsection

@section('popular_tag')
    n/a
@endsection
