
@extends('layoutHome.default')

@section('content')

    <div class="popular_tag">

        <dl>
            <dt>ID</dt>
            <dd>{{$popular_tag->id}}</dd>

            <dt>Name</dt>
            <dd>{{$popular_tag->name}}</dd>

            <dt>Link</dt>
            <dd>{{$popular_tag->link}}</dd>

            <dt>Soft Delete</dt>
            <dd>{{$popular_tag->soft_delete}}</dd>

            <dt>Is Draft</dt>
            <dd>{{$popular_tag->is_draft}}</dd>

        </dl>

    </div>
@endsection

@section('popular_tag')
    n/a
@endsection
