{{--
Hi I am created.blade.php file--}}



@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{url('popular_tag/'.$popular_tag->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        {{method_field('put')}}


        <div class="form-group">
            <label class = "control-label" for="name">Name</label>
            <input id="name" type="text" name="name" value="{{$popular_tag->name}}" placeholder="write your name"  class="form-control" required>
        </div>

        <div class="form-group">
            <label class = "control-label" for="link">Link</label>
            <input id="link" type="text" name="link" value="{{$popular_tag->link}}" placeholder=""  class="form-control" required>
        </div>


        <div class="form-group">
            <label class = "control-label" for="soft_delete">Soft Delete</label>
            <input id="soft_delete" type="number" name="soft_delete" value="{{$popular_tag->soft_delete}}" placeholder=""  class="form-control" required>
        </div>

        <div class="form-group">
            <label class = "control-label" for="is_draft">Is Draft</label>
            <input id="is_draft" type="number" name="is_draft" value="{{$popular_tag->is_draft}}" placeholder=""  class="form-control" required>
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>

    </form>

@endsection

@section('popular_tag')
    n/a
@endsection
