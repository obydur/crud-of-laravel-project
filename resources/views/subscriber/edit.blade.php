{{--
Hi I am created.blade.php file--}}



@extends('layoutHome.default')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            Subscriber Create :
                        </div>
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{Session::get('success')}}</div>
                        @endif

                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif



                            <form action="{{url('subscriber/'.$subscriber->id)}}" method="post" enctype="multipart/form-data">

                        @csrf

                                {{ method_field('put') }}

                            <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class = "control-label" for="email">Email Id</label>
                                                <input id="email" type="email" name="email" value="{{$subscriber->email}}" placeholder=""  class="form-control" required>
                                            </div>
                                        </div>
                                    </div>


                                <div class="row">
                                    <div class="col-md-8">
                                <div class="form-group">
                                    <label>Is Active? </label>
                                    @if($subscriber->is_active==1)
                                        <input type="radio" name="is_active" value="1" checked>Yes
                                        <input type="radio" name="is_active" value="0">No
                                    @else
                                        <input type="radio" name="is_active" value="1">Yes
                                        <input type="radio" name="is_active" value="0" checked>No
                                    @endif
                                </div>
                                </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class = "control-label" for="reason">Reason</label>
                                            <textarea name="reason" rows="1" cols="20" placeholder="Your Reason..." value="{{$subscriber->reason}}" class="form-control" ></textarea>
                                        </div>
                                    </div>
                                </div>


                                <button class="btn btn-success" type="submit">Add Post</button>
                                <button class="btn btn-success" type="reset">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@section('contact')
    n/a
@endsection
