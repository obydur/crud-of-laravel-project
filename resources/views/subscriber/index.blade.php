
@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <a href="subscriber/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">

        <thead class="">
        <tr>
            <th>ID</th>
            <th>Email Id</th>
            <th>Is Subscriber</th>
            <th>Reason</th>
            <th>Action</th>

        </tr>
        </thead>

        @php
            $sl = 0;
        @endphp

        @foreach($subscribers as $subscriber)
            <tr>
                <td>{{$sl++}}</td>
                <td><a href="{{route('subscriber.show',['id'=>$subscriber->id])}}">{{$subscriber->email}}</a> </td>
                <td><a href="{{route('subscriber.show',['id'=>$subscriber->id])}}">{{$subscriber->is_subscriber}}</a> </td>
                <td><a href="{{route('subscriber.show',['id'=>$subscriber->id])}}">{{$subscriber->reason}}</a> </td>
                <td><a href="{{route('subscriber.edit',$subscriber->id)}}">Edit</a> |

                    {!! Form::open(array('url' => ['$subscriber',$subscriber->id],'onclick'=>"return confirm('Are you sure you want to delete this item?');",'method' => 'DELETE')) !!}
                    {{--{!! Form::open(array('route' => ['labs.destroy',$subscriber->id],'method' => 'DELETE')) !!}--}}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}</td>
            </tr>

        @endforeach
    </table>

@endsection

@section('subscriber')
    n/a
@endsection
