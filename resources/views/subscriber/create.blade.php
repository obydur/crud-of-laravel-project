{{--
Hi I am created.blade.php file--}}



@extends('layoutHome.default')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            Subscriber Create :
                        </div>
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{Session::get('success')}}</div>
                        @endif

                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{url('/subscriber')}}" method="post" enctype="multipart/form-data">

                        @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class = "control-label" for="email">Email Id</label>
                                            <input id="email" type="email" name="email" value="" placeholder=""  class="form-control" >
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class = "control-label" for="is_subscriber">Is Subscriber</label>
                                            <input id="is_subscriber" type="radio" name="is_subscriber" value="1" placeholder=""  class="form-control" >Yes

                                            <input id="is_subscriber" type="radio" name="is_subscriber" value="2" placeholder=""  class="form-control" >No
                                        </div>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class = "control-label" for="reason">Reason</label>
                                            <textarea name="reason" rows="1" cols="20" placeholder="Your Reason..." class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>





                                <button class="btn btn-success" type="submit">Add Post</button>
                                <button class="btn btn-success" type="reset">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@section('subscriber')
    n/a
@endsection
