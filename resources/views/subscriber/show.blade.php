@extends('layoutHome.default')

@section('content')

    <div class="card">

        <dl>
            <dt>ID</dt>
            <dd>{{$subscriber->id}}</dd>

            <dt>Email</dt>
            <dd>{{$subscriber->email}}</dd>

            <dt>Is Subscriber</dt>
            <dd>{{$subscriber->is_subscriber}}</dd>

            <dt>Reason</dt>
            <dd>{{$subscriber->reason}}</dd>

            <dt>Created At</dt>
            <dd>{{$subscriber->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$subscriber->updated_at}}</dd>
        </dl>

    </div>
@endsection

@section('subscriber')
    n/a
@endsection
