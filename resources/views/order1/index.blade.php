
@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <a href="orders1/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">

        <thead class="">
        <tr>
            <th>Order ID</th>
            <th>SID</th>
            <th>Order Status</th>
            <th>Order Date</th>
            <th>Required Date</th>
            <th>Shipped Date</th>
            <th>Store ID</th>
            <th>Staff ID</th>
            <th>Action</th>

        </tr>
        </thead>

        @php
            $sl = 0;
        @endphp

        @foreach($orders1s as $orders1)
            <tr>
                <td>{{$sl++}}</td>
                <td><a href="{{route('orders1.show',['id'=>$orders1->id])}}">{{$orders1->sid}}</a> </td>
                <td>{{$ordes1->order_status}}</td>
                <td>{{$ordes1->order_date}}</td>
                <td>{{$ordes1->required_date}}</td>
                <td>{{$ordes1->shipped_date}}</td>
                <td>{{$ordes1->store_id}}</td>
                <td>{{$ordes1->staff_id}}</td>
                <td><a href="#">Edit</a> |<a href="#">Delete</a></td>
            </tr>
        @endforeach
    </table>

@endsection

@section('orders1')
    n/a
@endsection
