
@extends('layoutHome.default')

@section('content')

@if (session('message'))
<div class="alert alert-success">
    {{ session('message') }}
</div>
@endif

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<a href="order/create" class="btn btn-outline-info">Add New</a>
<table class="table table-bordered">

    <thead class="">
    <tr>
        <th>ID</th>
        <th>product_id</th>
        <th>qty</th>
        <th>Action</th>

    </tr>
    </thead>

    @php
    $sl = 0;
    @endphp

    @foreach($orders as $order)
    <tr>
        <td>{{$sl++}}</td>
        <td><a href="{{route('order.show',['id'=>$order->id])}}">{{$order->product_id}}</a> </td>
        <td>{{$order->qty}}</td>
        <td><a href="#">Edit</a> |<a href="#">Delete</a></td>
    </tr>
    @endforeach
</table>

@endsection

@section('order')
n/a
@endsection
