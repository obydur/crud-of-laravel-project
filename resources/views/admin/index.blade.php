
@extends('layoutHome.default')

@section('content')

    <a href="admin/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">
        <thead class="">
        <tr>
            <th>No.</th>
            <th>Name</th>
            <th>Email</th>
            <th>Password</th>
            <th>Phone</th>
            <th>Soft Delete</th>
            <th>Is Draft</th>
            <th>Action</th>
        </tr>
        </thead>


        @php
            $sl = 0;
        @endphp

        @foreach($admins as $admin)
            <tr>
                <td>{{++$sl}}</td>

                {{--<td><a href="{{url('/form/'.$form->id)}}">{{$form->firstname}}</a> </td>--}}
                <td><a href="{{route('admin.show',['id'=>$admin->id])}}">{{$admin->name}}</a> </td>
                <td>{{$admin->email}}</td>
                <td>{{$admin->password}}</td>
                <td>{{$admin->phone}}</td>
                <td>{{$admin->soft_delete}}</td>
                <td>{{$admin->is_draft}}</td>
                <td>
                    <a href="{{route('admin.edit',['id'=>$admin->id])}}">Edit </a> |
                    {!! Form::open(array('url' => ['admin',$admin->id],'onclick'=>"return confirm('Are you sure you want to delete this item?');",'method' => 'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}


                </td>
            </tr>
        @endforeach
    </table>

@endsection
