
{{--hi am admin pannel--}}


@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{url('admin/'.$admin->id)}}" method="post" enctype="multipart/form-data">
        @csrf

        {{method_field('put')}}
        <div class="form-group">
            <label class = "control-label" for="name">Name</label>
            <input id="name" type="text" name="name" value="{{$admin->name}}" placeholder="write your Name"  class="form-control">
        </div>

        <div class="form-group">
            <label class = "control-label" for="email">Email</label>
            <input id="email" type="text" name="email" value="{{$admin->email}}" placeholder="Please type your email id"  class="form-control">
        </div>


        <div class="form-group">
            <label class = "control-label" for="password">Password</label>
            <input id="password" type="password" name="password" value="{{$admin->password}}" placeholder=""  class="form-control" >
        </div>



        <div class="form-group">
            <label class = "control-label" for="phone">Phone No</label>
            <input id="phone" type="number" name="phone" value="{{$admin->phone}}" placeholder=""  class="form-control">
        </div>


        <div class="form-group">
            <label class = "control-label" for="soft_delete">Soft Delete</label>
            <input id="soft_delete" type="number" name="soft_delete" value="{{$admin->soft_delete}}" placeholder=""  class="form-control" >
        </div>

        <div class="form-group">
            <label class = "control-label" for="is_draft">Is Draft</label>
            <input id="is_draft" type="number" name="is_draft" value="{{$admin->is_draft}}" placeholder=""  class="form-control" >
        </div>

         <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>

    </form>

@endsection

@section('admin')
    n/a
@endsection
