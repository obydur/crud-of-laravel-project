@extends('layoutHome.default')

@section('content')

    <div class="card">

        <dl>
            <dt>ID</dt>
            <dd>{{$admin->id}}</dd>

            <dt>Name</dt>
            <dd>{{$admin->name}}</dd>

            <dt>Email</dt>
            <dd>{{$admin->email}}</dd>

            <dt>Password</dt>
            <dd>{{$admin->password}}</dd>

            <dt>Phone</dt>
            <dd>{{$admin->phone}}</dd>

            <dt>Soft Delete</dt>
            <dd>{{$admin->soft_delete}}</dd>

            <dt>Is Draft</dt>
            <dd>{{$admin->is_draft}}</dd>

            <dt>Created At</dt>
            <dd>{{$admin->created_at}}</dd>

            <dt>Updated At</dt>
            <dd>{{$admin->updated_at}}</dd>
        </dl>

    </div>
@endsection

@section('admin')
    n/a
@endsection
