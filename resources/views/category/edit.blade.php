{{--
Hi I am created.blade.php file--}}



@extends('layoutHome.default')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{url('category/'.$category->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        {{method_field('put')}}


        <div class="form-group">
            <label class = "control-label" for="name">Name</label>
            <input id="name" type="text" name="name" value="{{$category->name}}" placeholder="write your name"  class="form-control" required>
        </div>

        <div class="form-group">
            <label class = "control-label" for="link">Link</label>
            <input id="link" type="text" name="link" value="{{$category->link}}" placeholder=""  class="form-control" required>
        </div>


        <div class="form-group">
            <label class = "control-label" for="soft_delete">Soft Delete</label>
            <input id="soft_delete" type="number" name="soft_delete" value="{{$category->soft_delete}}" placeholder=""  class="form-control" required>
        </div>

        <div class="form-group">
            <label class = "control-label" for="is_draft">Is Draft</label>
            <input id="is_draft" type="number" name="is_draft" value="{{$category->is_draft}}" placeholder=""  class="form-control" required>
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>

    </form>

@endsection

@section('category')
    n/a
@endsection
