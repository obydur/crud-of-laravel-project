
@extends('layoutHome.default')

@section('content')

    <div class="category">

        <dl>
            <dt>ID</dt>
            <dd>{{$category->id}}</dd>

            <dt>Name</dt>
            <dd>{{$category->name}}</dd>

            <dt>Link</dt>
            <dd>{{$category->link}}</dd>

            <dt>Soft Delete</dt>
            <dd>{{$category->soft_delete}}</dd>

            <dt>Is Draft</dt>
            <dd>{{$category->is_draft}}</dd>

        </dl>

    </div>
@endsection

@section('category')
    n/a
@endsection
