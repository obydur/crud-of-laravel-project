

@extends('layoutHome.default')

@section('content')

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <a href="category/create" class="btn btn-outline-info">Add New</a>
    <table class="table table-bordered">

        <thead class="">
        <tr>
            <th>ID</th>
            <th>name</th>
            <th>Link</th>
            <th>Soft Delete</th>
            <th>Is Draft</th>
            <th>Action</th>

        </tr>
        </thead>

        @php
            $sl = 9;
        @endphp

        @foreach($categories as $category)
            <tr>
                <td>{{++$sl}}</td>

                <td><a href="{{route('category.show',['id'=>$category->id])}}">{{$category->name}}</a> </td>
                <td><a href="{{route('category.show',['id'=>$category->id])}}">{{$category->link}}</a> </td>
                <td>{{$category->soft_delete}}</td>
                <td>{{$category->is_draft}}</td>
                <td>
                    <a href="{{route('category.edit', $category->id)}}">Edit</a> |
                    {!! Form::open(array('url' => ['category',$category->id],'onclick' =>"return confirm('Are you sure you want to delete this data form the data table'); ",'method'=>'DELETE')) !!}
                    <button type="submit" class="btn btn-primary">Delete</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>

@endsection

@section('category')
    n/a
@endsection
