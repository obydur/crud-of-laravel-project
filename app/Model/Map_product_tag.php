<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Map_product_tag extends Model
{
    protected $fillable = ['product_id','tag_id'];
}
