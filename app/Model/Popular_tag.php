<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Popular_tag extends Model
{
    protected $fillable=  ['name','link','soft_delete','is_draft'];
}
