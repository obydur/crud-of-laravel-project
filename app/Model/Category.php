<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $fillable = ['name','link','soft_delete','is_draft'];
}
