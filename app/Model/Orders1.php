<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Orders1 extends Model
{
    protected $fillable = ['order_id','sid','order_status','order_date','required_date','shipped_date','store_id', 'staff_id'];
}
