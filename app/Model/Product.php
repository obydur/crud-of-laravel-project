<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['category_id', 'product_title','picture','short_description','description','additional_information','price','special_price','offer','start_date','end_date','sku','product_url','created_by','updated_by'];
}
