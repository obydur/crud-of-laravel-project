<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $fillable = ['firstname', 'lastname','password','date_of_birth','skill','gender','car','message'];
}
