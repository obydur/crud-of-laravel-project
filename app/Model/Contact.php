<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $fillable = ['name','email','subject','comment','status','soft_delete','datetime'];

}
