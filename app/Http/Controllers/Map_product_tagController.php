<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

Use App\Model\Map_product_tag;

/* $orders = Order::all();
        return view('order/index', compact('orders'));
*/

class Map_product_tagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $map_product_tags = Map_product_tag::all();
        return view('map_product_tag.index',compact('map_product_tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('map_product_tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            //dd($request);
            $data = $request->all();
            //dd($data);
           Map_product_tag::create($data);
           return redirect()->route('map_product_tag.index')->with('message','Map is Updated successfully');
        }catch (QueryException $exception){
            return redirect()->route('map_product_tag.create')->withInput->withErrors($exception->getMessage());
        }


//       // dd($request);
//        $data = $request->all();
//        //dd($data);
//       Map_product_tag::create($data);
//       return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Map_product_tag $map_product_tag)
    {
       //dd($map_product_tag);
        return view('map_product_tag.show',compact('map_product_tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Map_product_tag $map_product_tag)
    {
        //dd($map_product_tag);
        return view('map_product_tag.edit', compact('map_product_tag'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Map_product_tag $map_product_tag)
    {
        //dd($request);

        try{
            //$map_product_tag = Map_product_tag::findOrFail($id);
            $data = $request->all();
            $map_product_tag->Update($data);
            return redirect()->route('map_product_tag.index')->with('message','Mapproduct is Updated Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('$map_product_tag.create')->withErrors($exception->getMessage());
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Map_product_tag $map_product_tag)
    {
        try{
            //$map_product_tag = Map_product_tag::findOrFail($id);
            $map_product_tag->Delete();
            return redirect()->route('map_product_tag.index')->with('message','Map product is deleted Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('map_product_tag.create')->withErrors($exception->getMessage());
        }
    }
}
