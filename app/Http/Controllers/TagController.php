<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Tag;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags= Tag::all();
       // dd($tags);
        return view('tag/index',compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tag/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        //$data = $request->all();
       // dd($data);
        //$result = Tag::create($data);
        //dd($result);
        //Tag::create($data);

        $tag = new Tag();
        $tag->title = $request['title'];
        //dd($tag);
        $tag->save();

        return back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function show($id)
    public function show(Tag $tag)

    {
         //$tag = Tag::findOrall($id);
        return view('tag/show',compact('tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function edit($id)
    public function edit(Tag $tag)
    {
        //$tag = Tag::findOrfail($id);
        //dd($tag);
        return view('tag/edit',compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        try{
            //$tag = Tag::findOrFail($id);
            $data = $request->all();
            $tag->Update($data);
            return redirect()->route('tag.index')->with('message','Tag is Updated Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('tag.create')->withErrors($exception->getMessage());
        }



        //dd($rana);
        //return view('tag.show',compact('tag'))
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Tag $tag)
    {

        try{
            //$tag = Tag::findOrFail($id);
            $tag->Delete();
            return redirect()->route('tag.index')->with('message','Tag is deleted Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('tag.create')->withErrors($exception->getMessage());
        }

    }
}



