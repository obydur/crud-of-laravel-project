<?php

namespace App\Http\Controllers;

use App\Model\Form;
use Illuminate\Http\Request;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forms = Form::all();
        //dd($form);
        return view('form/index',compact('forms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('form/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        //$data = $request->all();
        //dd($data);
        //Form::create($data);

        $form = new Form();

        //$form->firstname = $_POST['first-name'];
        $form->firstname = $request['first-name'];
        $form->lastname = $request['last-name'];
        $form->password = $request['password'];
        $form->date_of_birth = $request['date_of_birth'];
        $form->skill = $request['skill'];
        $form->gender = $request['gender'];
        $form->car = $request['car-brand'];
        $form->message = $request['massage'];

        //dd($form);
        $form->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //public function show(Form $form)
   public function show(Form $form)

   {
       return view('form/show',compact('form'));

   }

       // return view('form.show',compact(  'form'));

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //return view('');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
