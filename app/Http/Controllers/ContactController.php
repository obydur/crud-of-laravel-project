<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Contact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::all();
       //dd($contacts);
        return view('contact/index',compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request);

        //$data = $request->all();
        //dd($data);
       // contact::create($data);

        $contact = new Contact();

        $contact->name = $request['name'];
        $contact->email= $request['email'];
        $contact->phone_no= $request['phone_no'];
        $contact->subject= $request['subject'];
        $contact->comment = $request['comment'];
        $contact->status = $request['status'];
        $contact->soft_delete = $request['soft_delete'];

        //dd($contact);
        $contact->save();

        return back();
    }

    /*
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   // public function show($id)
   public function show(Contact $contact)

    {
        //$contact = Contact::findorfail($id);
        return view('contact/show',compact('contact'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        return view('contact.edit',compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //dd($request);

        try{
            //$contact = Contact::findOrFail($id);
            $data = $request->all();
            $contact->Update($data);
            return redirect()->route('contact.index')->with('message','Contact is Updated Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('contact.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        try{
            //$contact= Contact::findOrFail($id);
            $contact->Delete();
            return redirect()->route('contact.index')->with('message','Contact is deleted Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('contact.create')->withErrors($exception->getMessage());
        }
    }
}
