<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Model\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('product/index', compact('products'));

        /*$products= Product::all();
        //dd($products);
        return view('product/index',compact('products'));*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            //dd($request->input());
            if($request->hasFile('picture'))
            {
                $data = $request->all();
                $fileName = $request->title.'-'.$request->picture->getClientOriginalName();
                $request->picture->move(public_path('/images/'), $fileName);
                $data['picture']=$fileName;
            }
            else {
                $data['picture'] = null;
            }

            Product::create($data);

            //return redirect()->route('product.index')->withMessage('Product is Inserted Successfully.');
            return redirect()->route('product.index')->with('message','product is added successfully.');
        }

        catch (QueryException $exception) {
            return redirect()->route('product.create')->withInput()->withErrors($exception->getMessage());
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Product $product)
    {
        //$product = Product::findOrall($id);
        return view('product/show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   //public function edit($id)
   public function edit(Product $product)
      {
          //$product = Product::findOrfail($id);
         // dd($product);
       return view('product/edit',compact('product'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            //dd($request->input());
            if($request->hasFile('picture'))
            {
                $data = $request->all();
                $fileName = $request->title.'-'.$request->picture->getClientOriginalName();
                $request->picture->move(public_path('/images/'), $fileName);
                $data['picture']=$fileName;
            }
            else {
                $data['picture'] = null;
            }

            Product::create($data);

            //return redirect()->route('sponser.index')->withMessage('product is Inserted Successfully.');
            return redirect()->route('product.index')->with('message','product is added successfully.');
        }

        catch (QueryException $exception) {
            return redirect()->route('product.create')->withInput()->withErrors($exception->getMessage());
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Product $product)
    {
        try{
            //$product = Product::findOrFail($id)
                $product->Delete();
            return redirect()->route('product.index')->with('message', 'product is deleted Successfully.');

        }catch (QueryException $exception){
            return redirect()->route('product.create')->withErrors($e ->getMessage());
        }
    }

}


