<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Sponser;

class SponserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $sponsers = Sponser::all();
            return view('sponser.index', compact('sponsers'));

        } catch (QueryException $exception) {
            return redirect()->route('404_blade')->withInput()->withErrors($e->getMessage());
        }

        /*$sponsers=Sponser::all();
        return view ('sponser.index',compact('sponsers'));*/


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sponser.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            //dd($request->input());
            if($request->hasFile('picture'))
            {
                $data = $request->all();
                $fileName = $request->title.'-'.$request->picture->getClientOriginalName();
                $request->picture->move(public_path('/images/'), $fileName);
                $data['picture']=$fileName;
            }
            else {
                $data['picture'] = null;
            }

            Sponser::create($data);

            //return redirect()->route('sponser.index')->withMessage('Sponser is Inserted Successfully.');
            return redirect()->route('sponser.index')->with('message','Sponser is added successfully.');
        }

        catch (QueryException $exception) {
            return redirect()->route('sponser.create')->withInput()->withErrors($exception->getMessage());
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //public function show($id)
    public function show(Sponser $sponser)
    {
        //$sponser = Sponser::findOrall($id);
        return view('sponser/show',compact('sponser'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('sponser/edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request, $id)
    {
        try{
            //$sponser = Sponser::findOrFail($id);
            $data = $request->all();
            $sponser->Update($data);
            return redirect()->route('sponser.index')->with('message','Sponser is Updated Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('sponser.create')->withErrors($exception->getMessage());
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Sponser $sponser)
    {

        try{
            //$sponser = Tag::findOrFail($id);
            $sponser->Delete();
            return redirect()->route('sponser.index')->with('message','Sponser is deleted Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('sponser.create')->withErrors($exception->getMessage());
        }




    }
}
