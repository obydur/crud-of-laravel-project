<?php

namespace App\Http\Controllers;

use App\Model\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('order/index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('order/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $data = $request->all();
            Order::create($data);
            return redirect()->route('order.index')->withStatus('Created Successfully!');
        }catch (QueryException $exception){

            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }

        /*$data = $request->all();
        //dd($data);
        Order::create($data);
        return back();*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //$order = order::findorall($id);
        return view('order/show',compact('order'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //$order = Order::findOrFail($id);
        return view('order/edit',compact('order'));

    }

    /* public function edit(Page $page)
    {
        // $page = Page::findOrfail($id);
        return view('page/edit',compact('page'));
    }*/


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //dd($request);
        try{
            $data = $request->all();
            $order->Update($data);
            return redirect()->route('order.index')->with('message','Order is Updated Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('order.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //dd($id);
       // dd($order);
        try{
           // $order = Order::findOrFail($id);
            $order->Delete();
            return redirect()->route('order.index')->with('message','Order is Deleted Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('order.create')->withErrors($exception->getMessage());
        }

    }
}
