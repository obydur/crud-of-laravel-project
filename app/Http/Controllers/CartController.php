<?php


namespace App\Http\Controllers;
use App\Http\Requests\CartRequest;
use Illuminate\Http\Request;
use App\Model\Cart;
use Illuminate\Database\QueryException;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts=Cart::all();
        //dd($carts);
       return view('cart.index',compact('carts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cart/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try{
                //dd($request->input());
                if($request->hasFile('picture'))
                {
                    $data = $request->all();
                    $fileName = $request->title.'-'.$request->picture->getClientOriginalName();
                    $request->picture->move(public_path('/images/'), $fileName);
                    $data['picture']=$fileName;
                }
                else {
                    $data['picture'] = null;
                }

                Cart::create($data);

                //return redirect()->route('labs.index')->withMessage('Lab is Inserted Successfully.');
                return redirect()->route('cart.index')->with('message','Cart is added successfully.');
            }

        catch (QueryException $exception) {
                return redirect()->route('cart.create')->withInput()->withErrors($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {

        // $cart = Cart::findorfail($id);
        return view('cart/show',compact('cart'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
       // $cart = Cart::findOrfail($id);
        return view('cart/edit',compact('cart'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   /* public function update(Request $request, $id)
    {
        //
    }*/

    public function update(CartRequest $request, Cart $cart)
    {
        try {

//            $cart = Cart::findOrFail($id);
            $data = $request->all();
            $oldpath=public_path('picture/' . $cart->picture);

            if($request->hasFile('picture')){
                $picture=$request->file('picture');
                $filename=time(). '.' .$picture->getClientOriginalExtension();
                $location=public_path('/picture/');
                $picture->move($location, $filename);
                $data['picture']=$filename;

            }
            else if($oldpath)
            {
                $data['picture']=$cart->picture;
            }

            $cart->update($data);
            return redirect('cart')->with('message','picture Updated !');
        }
        catch(QueryException $e){

            return redirect()
                ->route('picture.create')
                ->withInput()
                ->withErrors($e->getMessage());
        }

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        try{
//            $cart=Cart::findOrFail($id);

            $cart->delete();
            return redirect()->route('cart.index')->with('message','cart is Deleted Successfully.');
        }catch(QueryException $e){
            return redirect()
                ->route('cart.index')
                ->withErrors($e->getMessage());
        }
    }
}
