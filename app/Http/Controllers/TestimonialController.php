<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Testimonial;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $testimonials= Testimonial::all();
       // dd($testimonials);
        return view('testimonial/index',compact('testimonials'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('testimonial/create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        try{
            //dd($request->input());
            if($request->hasFile('picture'))
            {
                $data = $request->all();
                $fileName = $request->title.'-'.$request->picture->getClientOriginalName();
                $request->picture->move(public_path('/images/'), $fileName);
                $data['picture']=$fileName;
            }
            else {
                $data['picture'] = null;
            }

            Testimonial::create($data);

            //return redirect()->route('labs.index')->withMessage('Lab is Inserted Successfully.');
            return redirect()->route('testimonial.index')->with('message','Testimonial is added successfully.');
        }

        catch (QueryException $exception) {
            return redirect()->route('testimonial.create')->withInput()->withErrors($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Testimonial $testimonial)
    {
        //$testimonial = Testimonial::findOrall($id);
        return view('testimonial/show',compact('testimonial'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('testimonial/edit',compact('testimonial'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial $testimonial)
    {
//        $updateRequest = $request->all();
//        dd($updateRequest);

        $oldPicture = $testimonial->picture;
        //dd($oldPicture);

        try{

            //$testimonial = Testimonial::findOrFail($id);
            $data = $request->all();
            if($request->hasFile('picture')){
                $fileName = $request->name.'pictureName-'.$request->picture->getClientOriginalName();
                if(file_exists('images/'.$oldPicture)){
                    unlink('images/'.$oldPicture);
                }
                $request->picture->move('images/', $fileName);

                $data['picture'] = $fileName;
            }
            $testimonial->Update($data);
            return redirect()->route('testimonial.index')->with('message','Testimonial is Updated Successfully.');

        }catch (QueryException $exception) {
            return redirect()->route('testimonial.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        try{
            //$testimonial = Testimonial::findOrFail($id);
            $testimonial->Delete();
            return redirect()->route('testimonial.index')->with('message','Testimonial is deleted Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('testimonial.create')->withErrors($exception->getMessage());
        }
    }
}
