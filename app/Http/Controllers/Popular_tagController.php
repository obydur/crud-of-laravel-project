<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Popular_tag;

class Popular_tagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $popular_tags = Popular_tag::all();
        //dd($popular_tags);
        return view('popular_tag/index',compact('popular_tags'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('popular_tag/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        //$data = $request->all();
        //dd($data);
        // contact::create($data);

        $popular_tag = new Popular_tag();

        $popular_tag->name = $request['name'];
        $popular_tag->link= $request['link'];
        $popular_tag->soft_delete = $request['soft_delete'];
        $popular_tag->is_draft = $request['is_draft'];

        //dd($popular_tag);
        $popular_tag->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function show($id)
    public function show(Popular_tag $popular_tag)

    {
        //$popular_tag = popular_tag::findorfail($id);
        return view('popular_tag/show',compact('popular_tag'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Popular_tag $popular_tag)
    {
        return view('popular_tag/edit',compact('popular_tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Popular_tag $popular_tag)
    {
        //dd($request);
        try{
            $data = $request->all();
            $popular_tag->update($data);
            return redirect()->route('popular_tag.index')->with('message','Popular Tag is updated succesfully.');
        }catch (QueryException $exception){
            return redirect()->route('popular_tag.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Popular_tag $popular_tag)
    {

        // dd($popular_tag);

        try{
            //$popular_tag = Popular_tag::findOrFail($id);
            $popular_tag->Delete();
            return redirect()->route('popular_tag.index')->with('message','Popular Tag is deleted Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('popular_tag.create')->withErrors($exception->getMessage());
        }
    }
}
