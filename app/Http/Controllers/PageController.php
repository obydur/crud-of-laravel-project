<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Page;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages=Page::all();
        //dd($pages);
        return view('page/index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request);

       // $data = $request->all();
        //dd($data);
        //page::create($data);

        $page = new Page();
        $page->page_title = $request['page_title'];
        $page->page_content= $request['page_content'];
        //dd($page);
        $page->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function show($id)
    public function show(Page $page)
    {
       // $page = Page::findOrall($id);
       return view('page/show',compact('page'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function edit($id)

    public function edit(Page $page)
    {
        // $page = Page::findOrfail($id);
        return view('page/edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        //dd($request);

       try{
           $data =$request->all();
           $page->Update($data);
           return redirect()->route('page.index')->with('message','Page is updated Successfully.');

        }catch (QueryException $exception){
           return redirect()->route('page.create')->withErrors($exception->getMessage());
       }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //dd($page);

        try{
            $page->Delete();
            return redirect()->route('page.index')->with('message','Page is deleted Successfully.');

        }catch (QueryException $exception){
            return redirect()->route('page.create')->withErrors($exception->getMessage());
        }
    }
}
