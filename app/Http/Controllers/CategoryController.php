<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        //dd($categories);
        return view('category/index',compact('categories'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        //$data = $request->all();
        //dd($data);
        // contact::create($data);

        $category = new Category();

        $category->name = $request['name'];
        $category->link= $request['link'];
        $category->soft_delete = $request['soft_delete'];
        $category->is_draft = $request['is_draft'];

        //dd($category);
        $category->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     //public function show($id)
   public function show(Category $category)

    {
        //$contact = Category::findorfail($id);
        return view('category/show',compact('category'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('category/edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Category $category)
    {
        //dd($request);
        try{
            $data = $request->all();
            $category->update($data);
            return redirect()->route('category.index')->with('message','Category is updated succesfully.');
        }catch (QueryException $exception){
            return redirect()->route('category.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {

       // dd($category);

        try{
            //$category = Category::findOrFail($id);
            $category->Delete();
            return redirect()->route('category.index')->with('message','Category is deleted Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('category.create')->withErrors($exception->getMessage());
        }
    }
}
