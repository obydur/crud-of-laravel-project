<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Label;

class LabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labels=Label::all();
        //dd($labels);
        return view('label/index', compact('labels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('label/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            //dd($request->input());
            if($request->hasFile('picture'))
            {
                $data = $request->all();
                $fileName = $request->title.'-'.$request->picture->getClientOriginalName();
                $request->picture->move(public_path('/images/'), $fileName);
                $data['picture']=$fileName;
            }
            else {
                $data['picture'] = null;
            }
            Label::create($data);
            //return redirect()->route('labs.index')->withMessage('Lab is Inserted Successfully.');
            return redirect()->route('label.index')->with('message','picture is Inserted Successfully.');
        } catch(QueryException $exception){
            return redirect()->route('label.create')->withInput()->withErrors($e->getMessage());
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Label $label)
    {
        //$label = Label::findorfail($id);
        return view('label/show',compact('label'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function edit($id)
    public function edit(Label $label)
    {
        // $cart = Cart::findOrfail($id);
        return view('label/edit',compact('label'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Label $label)
    {
        try{
            //$label = Label::findOrFail($id);
            $data = $request->all();
            $label->Update($data);
            return redirect()->route('label.index')->with('message','Label is Updated Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('label.create')->withErrors($exception->getMessage());
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Label $label)
    {
        try{
            //$label = Label::findOrFail($id);
            $label->Delete();
            return redirect()->route('label.index')->with('message','Label is deleted Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('label.create')->withErrors($exception->getMessage());
        }
    }
}
