<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Subscriber;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscribers = Subscriber::all();
        return view('subscriber/index',compact('subscribers'));

    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subscriber/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        try{
            $data = $request->all();
            Subscriber::create($data);
            return back();

        }catch (QueryException $exception)
        {
            return redirect()->route('subscriber/create')->withInput()->withErrors($exception->getMessage());
            }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show(Subscriber $subscriber)
    {
       //$subscriber =Subscriber::findOrFail($id);
        return view('subscriber/index',compact('subscriber'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Subscriber $subscriber)
    {
       //$subscriber = Subscriber::findOrfail($id);
        //dd($subscriber)
        return view('subscriber/edit',compact('subscriber'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request,Subscriber $subscriber)
    {
        try{
            //$subscriber = Subscriber::findOrFail($id);
            $data = $request->all();
            $subscriber->Update($data);
            return redirect()->route('subscriber.index')->with('message','Subscriber is Updated Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('subscriber.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Subscriber $subscriber)
    {
        try{
            //$subscriber =Subscriber::findOrFail($id);
            $subscriber->delete();
            return redirect()->route('subscriber.index')->with('message','Subscriber Deleted Successfully.');
        }
        catch(QueryException $exception)
        {
            return redirect()->route('subscriber.create')->withErrors($exception->getMessage());
        }

    }



}
