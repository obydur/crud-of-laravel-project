<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Brand;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
       // dd($brands);
        return view('brand/index',compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brand/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        //$data = $request->all();
        //dd($data);
       // brand::create($data);/

        try{
            $data = $request->all();
            Brand::create($data);
            return back();
        }catch (QueryException $exception){
            return redirect()->route('brand/create')->withInput()->withErrors($exception->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        return view('brand/show',compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function edit($id)
    public function edit(Brand $brand)
    {
        // $brand = Brand::findOrfail($id);
        return view('brand/edit',compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        try{
            //$brand = Brand::findOrFail($id);
            $data = $request->all();
            $brand->Update($data);
            return redirect()->route('brand.index')->with('message','Brand is Updated Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('brand.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        try{
            //$brand = Brand::findOrFail($id);
            $brand->Delete();
            return redirect()->route('brand.index')->with('message','Brand is deleted Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('brand.create')->withErrors($exception->getMessage());
        }
    }
}
