<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Banner;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::all();

        return view('banner/index',compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('banner/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            //dd($request->input());
            if($request->hasFile('picture'))
            {
                $data = $request->all();
                $fileName = $request->title.'-'.$request->picture->getClientOriginalName();
                $request->picture->move(public_path('/images/'), $fileName);
                $data['picture']=$fileName;
            }
            else
            {
                $data['picture'] = null;
            }

            Banner::create($data);

            //return redirect()->route('labs.index')->withMessage('Lab is Inserted Successfully.');
            return redirect()->route('banner.index')->with('message','Banner is Inserted Successfully.');
        }
        catch(QueryException $e)
        {
            return redirect()->route('banner.create')->withInput()->withErrors($e->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Banner $banner)
    {
        return view('banner/show', compact('banner'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Banner $banner)
    {

        return view('banner/edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Banner $banner)
    {
        try{
            //dd($request->input());
            $data = $request->all();
            $oldpath = public_path('images/'.$banner->picture);
            if($request->hasFile('picture'))
            {
                $image = $request->file('picture');
                $fileName =time().'-'.$image->getClientOriginalExtension();
                $location = public_path('/images/');
                $image->move($location,$fileName);
                $data['picture']=$fileName;
            }
            else if($oldpath){
                $data['picture']= $banner->picture;
            }

            $banner->update($data);
            return redirect()->route('banner.index')->with('message','Banner is added successfully.');
        }catch (QueryException $exception){
            return redirect()->route('banner.create')->withInput()->withErrors($exception->getMessage());
        }

    }


       /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
       // dd($banner);
        try{
            $banner->Delete();
            return redirect()->route('banner.index')->with('message','Banner is deleted Successfully.');

        }catch (QueryException $exception){
            return redirect()->route('banner.create')->withInput()->withErrors($exception->getMessage());
        }

    }
}
