<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Admin;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::all();
        //dd($admins);
        return view('admin/index',compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        //$data = $request->all();
        //dd($data);
        //Admin::create($data);

        $admin = new Admin();

        //$form->firstname = $_POST['first-name'];
        $admin->name = $request['name'];
        $admin->email = $request['email'];
        $admin->password = $request['password'];
        $admin->phone = $request['phone'];
        $admin->soft_delete = $request['soft_delete'];
        $admin->is_draft = $request['is_draft'];

        //dd($admin);
        $admin->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        return view('admin/show',compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        return view('admin.edit',compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
            //dd($request);

            try{
            //$admin = Admin::findOrFail($id);
            $data = $request->all();
            $admin->Update($data);
            return redirect()->route('admin.index')->with('message','Admin is Updated Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('admin.create')->withErrors($exception->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        try{
            //$admin = Admin::findOrFail($id);
            $admin->Delete();
            return redirect()->route('admin.index')->with('message','Admin is deleted Successfully.');
        }catch (QueryException $exception){
            return redirect()->route('admin.create')->withErrors($exception->getMessage());
        }

    }
}
